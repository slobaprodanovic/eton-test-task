<?php

namespace Drupal\eton_test\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "cron_applicant_info_email",
 *   title = @Translation("Learning task worker: email queue"),
 *   cron = {"time" = 60}
 * )
 */
class CronApplicantInfoEmail extends QueueWorkerBase {

  /**
   * Sends individual email.
   */
  public function processItem($data) {
    $data = (array) $data;

    $mail_manager = \Drupal::service('plugin.manager.mail');
    $params = $data;
    $lang_code = \Drupal::currentUser()->getPreferredLangcode();
    $mail_manager->mail('eton_test', 'applicant_info_email', $data['email'], $lang_code, $params, TRUE);
  }

}
