<?php

namespace Drupal\eton_test\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;

/**
 * Creates form for job applicants to fill it out.
 */
class JobApplicationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eton_test_job_application_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Please enter your name.'),
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Please enter your email address.'),
    ];
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Please select what type of developer are you.'),
      '#default_value' => 2,
      '#options' => [
        '1' => 'Back-end',
        '2' => 'Front-end',
      ],
      '#ajax' => [
        'callback' => [$this, 'technologyAjaxCallback'],
        'event' => 'change',
        'wrapper' => 'edit-technology',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Updating technology...'),
        ],
      ],
    ];
    $form['technology'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Please choose type of technology that you prefer.'),
      '#prefix' => '<div id="edit-technology"',
      '#suffix' => '</div>',
      '#validated' => TRUE,
      '#options' => [
        '10' => 'AngularJS',
        '11' => 'ReactJS',
      ],
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Your message to Eton Digital.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Email should be already validated via form field 'email' type, this
    // is just in case.
    $email_value = $form_state->getValue('email');
    if (!\Drupal::service('email.validator')->isValid($email_value)) {
      $form_state->setErrorByName('email', $this->t('Email address is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conn = Database::getConnection();
    $values = $form_state->getValues();

    $data = $this->prepareDataForInsert($values);
    $result = $conn->insert('job_applications')->fields($data)->execute();

    // Check result of insert into db.
    if (is_numeric($result)) {
      // Send email about new applicant to website admin.
      $this->newJobApplicationEmailNotification($values);
      $this->messenger()->addMessage($this->t('Your application is received, thank you for your interest in our company.'), 'status');
    }
    else {
      \Drupal::logger('eton_test')->notice('Error while inserting user data into db. User data: <pre>@data</pre>',
        [
          '@data' => print_r($values, TRUE),
        ]
      );
      $this->messenger()->addMessage($this->t('Error while processing your data, please try again later.'), 'error');
    }
  }

  /**
   * Ajax callback for technology form element.
   */
  public function technologyAjaxCallback(array &$form, FormStateInterface $form_state) {
    $type_value = $form_state->getValue('type');

    // Back-end.
    if ($type_value == 1) {
      $options = [
        '1' => 'PHP',
        '2' => 'Java',
      ];
    }
    // Front-end.
    elseif ($type_value == 2) {
      $options = [
        '10' => 'AngularJS',
        '11' => 'ReactJS',
      ];
    }
    // Just in case.
    else {
      $options = ['0' => 'Unknown'];
      \Drupal::logger('eton_test')->warning('Please check code for Job Application form, there seems to be an error in logic.');
    }

    $form['technology']['#options'] = $options;
    return $form['technology'];
  }

  /**
   * Custom method. Sends email to site to inform about new application.
   *
   * @param array $data
   *   - Data that applicant has inserted via form.
   *
   * @return bool
   *   - If email is succesfully sent, it will return TRUE. Else, it will be
   *     FALSE returned.
   */
  protected function newJobApplicationEmailNotification(array $data) {
    $data = _eton_test_replace_integer_text_value_type_technology($data);

    $mail_manager = \Drupal::service('plugin.manager.mail');
    $module = 'eton_test';
    $key = 'new_job_application';
    $to = \Drupal::config('system.site')->get('mail');
    $lang_code = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mail_manager->mail($module, $key, $to, $lang_code, $data, NULL, $send);
    if ($result['result'] !== TRUE) {
      \Drupal::logger('eton_test')->notice('Error while sending email. Debug data: <pre>@data</pre>',
        [
          '@data' => print_r($result, TRUE),
        ]
      );
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Custom method. Prepares form data for insert into db.
   *
   * @param array $data
   *   - Data that is submitted via form.
   *
   * @return array
   *   - Processed data.
   */
  protected function prepareDataForInsert(array $data) {
    // Prepare variable for insertion timestamp.
    $dateTime = new DrupalDateTime();
    $timestamp = $dateTime->format('Y-m-d H:i:s');

    // Data about form is not needed in database.
    unset($data['submit'], $data['form_build_id'], $data['form_token'], $data['form_id'], $data['op']);
    $data['created'] = $timestamp;

    return $data;
  }
}
