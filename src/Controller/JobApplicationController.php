<?php

namespace Drupal\eton_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

/**
 * Controller that is used to manipulate job applicants data.
 */
class JobApplicationController extends ControllerBase {

  /**
   * Creates table containing job applicants data.
   */
  public function showTable() {
    $conn = Database::getConnection();

    $header = [
      ['data' => t('ID'), 'field' => 'id', 'sort' => 'asc'],
      ['data' => t('Name'), 'field' => 'name'],
      ['data' => t('Email'), 'field' => 'email'],
      ['data' => t('Type'), 'field' => 'type'],
      ['data' => t('Technology'), 'field' => 'technology'],
      ['data' => t('Message'), 'field' => 'message'],
    ];

    $query = $conn->select('job_applications', 'ja');
    $query->fields('ja', [
      'id',
      'name',
      'email',
      'type',
      'technology',
      'message',
    ]
    );
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(5);
    $result = $pager->execute();

    foreach ($result as $row) {
      $row = _eton_test_replace_integer_text_value_type_technology($row);
      $rows[] = ['data' => (array) $row];
    }

    $build = [
      '#markup' => t('List of job applications'),
    ];

    $build['job_applications_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('There are no applications yet.'),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
